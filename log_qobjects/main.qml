import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import libr 1.0

Window {
    visible: true
    width: 640
    height: 480

    DummyObject {
        id: dummy_object
        name: "test"
    }

    ExposedObject {
        id: exposed_object
        name: "exposed"
    }

    Row {
        Button {
            id: console_log
            text: "console log"
            onClicked: {
                console.log(dummy_object);
                console.log(exposed_object);
            }
        }

        Button {
            id: json_log
            text: "json log"
            onClicked: {
                console.log(JSON.stringify(dummy_object, null, 4));
                console.log(JSON.stringify(exposed_object, null, 4));
            }
        }
    }
}
