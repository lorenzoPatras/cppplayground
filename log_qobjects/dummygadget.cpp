#include "dummygadget.h"

DummyGadget::DummyGadget()
    : DummyGadget("prop0", "prop1", 0)
{
}

DummyGadget::DummyGadget(const QString& prop0, const QString& prop1, const int prop2)
    : prop0_(prop0)
    , prop1_(prop1)
    , prop2_(prop2)
{
}
