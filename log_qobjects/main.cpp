#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "dummyobject.h"
#include "dummygadget.h"
#include "exposedobject.h"

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<DummyObject>("libr", 1, 0, "DummyObject");
    qmlRegisterType<ExposedObject>("libr", 1, 0, "ExposedObject");
    qRegisterMetaType<DummyGadget>();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
