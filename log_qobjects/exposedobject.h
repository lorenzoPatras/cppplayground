#ifndef EXPOSEDOBJECT_H
#define EXPOSEDOBJECT_H

#include <QObject>
#include <QVariant>
#include <memory>

#include "dummygadget.h"

class ExposedObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

    Q_PROPERTY(DummyGadget gadget READ gadget WRITE setGadget NOTIFY gadgetChanged)

    Q_PROPERTY(QVariant var READ var)

public:
    ExposedObject(QObject* parent = nullptr);
    ~ExposedObject();

    void setName(const QString& name);

    QString name() const;

    void setGadget(const DummyGadget& gadget);

    DummyGadget gadget() const;

    QVariant var() const;

signals:
    void nameChanged();
    void gadgetChanged();

private:
    class ExposedObjectImpl;
    std::unique_ptr<ExposedObjectImpl> pImpl_;
};

#endif  // EXPOSEDOBJECT_H
