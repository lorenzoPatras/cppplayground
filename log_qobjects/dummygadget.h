#ifndef DUMMYGADGET_H
#define DUMMYGADGET_H

#include <QMetaType>
#include <QString>

class DummyGadget
{
    Q_GADGET

    Q_PROPERTY(QString prop0 READ prop0)

    Q_PROPERTY(QString prop1 READ prop1)

    Q_PROPERTY(int prop2 READ prop2)

public:
    DummyGadget();

    DummyGadget(const QString& prop0, const QString& prop1, const int prop2);

    QString prop0() const
    {
        return prop0_;
    }

    QString prop1() const
    {
        return prop1_;
    }

    int prop2() const
    {
        return prop2_;
    }

private:
    QString prop0_;
    QString prop1_;
    int     prop2_;
};
Q_DECLARE_METATYPE(DummyGadget)

#endif  // DUMMYGADGET_H
