#ifndef DUMMYOBJECT_H
#define DUMMYOBJECT_H

#include <QObject>
#include <QVariant>

#include "dummygadget.h"

class DummyObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

    Q_PROPERTY(DummyGadget gadget READ gadget WRITE setGadget NOTIFY gadgetChanged)

    Q_PROPERTY(QVariant var READ var)

    Q_PROPERTY(QVariantList variants READ variants)

public:
    DummyObject(QObject* parent = nullptr);

    void setName(const QString& name);

    QString name() const;

    void setGadget(const DummyGadget& gadget);

    DummyGadget gadget() const;

    QVariant var() const;

    QVariantList variants() const;

signals:
    void nameChanged();
    void gadgetChanged();

public slots:

private:
    QString     name_;
    DummyGadget gadget_;
};

#endif  // DUMMYOBJECT_H
