#include "exposedobject.h"

class ExposedObject::ExposedObjectImpl
{
public:
    ExposedObjectImpl()
    {
    }

    ~ExposedObjectImpl()
    {
    }

    void setName(const QString& name)
    {
        name_ = name;
    }

    QString name() const
    {
        return name_;
    }

    void setGadget(const DummyGadget& gadget)
    {
        gadget_ = gadget;
    }

    DummyGadget gadget() const
    {
        return gadget_;
    }

    QVariant var() const
    {
        return QVariant::fromValue(gadget_);
    }

private:
    QString     name_;
    DummyGadget gadget_;
};

ExposedObject::ExposedObject(QObject* parent)
    : QObject(parent)
    , pImpl_(new ExposedObjectImpl())
{
}

ExposedObject::~ExposedObject()
{
}

void ExposedObject::setName(const QString& name)
{
    pImpl_->setName(name);
    emit nameChanged();
}

QString ExposedObject::name() const
{
    return pImpl_->name();
}

void ExposedObject::setGadget(const DummyGadget& gadget)
{
    pImpl_->setGadget(gadget);
    emit gadgetChanged();
}

DummyGadget ExposedObject::gadget() const
{
    return pImpl_->gadget();
}

QVariant ExposedObject::var() const
{
    return pImpl_->var();
}
