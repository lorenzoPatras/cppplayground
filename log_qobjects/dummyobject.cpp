#include "dummyobject.h"

DummyObject::DummyObject(QObject* parent)
    : QObject(parent)
{
}

void DummyObject::setName(const QString& name)
{
    name_ = name;
    emit nameChanged();
}

QString DummyObject::name() const
{
    return name_;
}

void DummyObject::setGadget(const DummyGadget& gadget)
{
    gadget_ = gadget;
    emit gadgetChanged();
}

DummyGadget DummyObject::gadget() const
{
    return gadget_;
}

QVariant DummyObject::var() const
{
    return QVariant::fromValue(gadget_);
}

QVariantList DummyObject::variants() const
{
    QVariantList lst;
    for (int i = 0; i < 5; i++)
    {
        lst << QVariant::fromValue(DummyGadget());
    }
    return lst;
}
